import { Component, OnInit, Input } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { Setting } from '../../models/setting';
import { SettingAtribute} from '../../models/setting-atribute';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
@Input() setting: Setting;
@Input() atribute: SettingAtribute;


  constructor(private settingService: SettingsService) { }

  ngOnInit() {
    
  }

}
