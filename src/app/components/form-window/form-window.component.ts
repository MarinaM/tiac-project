import { Component, OnInit, Input} from '@angular/core';
import { Setting } from '../../models/setting';
import { SettingAtribute } from '../../models/setting-atribute';

@Component({
  selector: 'app-form-window',
  templateUrl: './form-window.component.html',
  styleUrls: ['./form-window.component.css']
})
export class FormWindowComponent implements OnInit {
  @Input() setting: Setting;
  @Input() atributes: SettingAtribute[];


  constructor() { }

  ngOnInit() {

  }
}
