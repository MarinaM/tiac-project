import { Component, OnInit, Input } from '@angular/core';
import { Element } from '../../model/element';

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.css']
})
export class FormViewComponent implements OnInit {
  @Input() element:Element;
 
  constructor() { }

  ngOnInit() {
  }

}
