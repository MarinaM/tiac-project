import { Component, OnInit, Input } from '@angular/core';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.css']
})
export class ColorComponent implements OnInit {
@Input() element:Element;
  constructor() { }

  ngOnInit() {
  }

}
