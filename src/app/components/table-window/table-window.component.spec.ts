import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableWindowComponent } from './table-window.component';

describe('TableWindowComponent', () => {
  let component: TableWindowComponent;
  let fixture: ComponentFixture<TableWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
