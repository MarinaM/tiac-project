import { Component, OnInit, Input } from '@angular/core';
import { Setting } from '../../models/setting';
import { SettingAtribute } from '../../models/setting-atribute';


@Component({
  selector: 'app-table-window',
  templateUrl: './table-window.component.html',
  styleUrls: ['./table-window.component.css']
})
export class TableWindowComponent implements OnInit {
  @Input() setting: Setting;

  constructor() { }

  ngOnInit() {
  }

}
