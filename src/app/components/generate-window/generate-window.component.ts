import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, OnChanges, SimpleChange, DoCheck, IterableDiffers, HostListener, Renderer2, SimpleChanges, ChangeDetectionStrategy, ChangeDetectorRef, KeyValueDiffers } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { Setting } from '../../models/setting';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-generate-window',
    templateUrl: './generate-window.component.html',
    styleUrls: ['./generate-window.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenerateWindowComponent implements DoCheck, OnInit {
    @ViewChild('canvas') public canvas: ElementRef;

    differ: any;
    objDiffer;

    _settings: Setting;
    @Input()
    set setting(value) {
        this._settings = value;
    }
    get setting() {
        return this._settings;
    }

    // store previous value of settings
    prevSettings;

    myArray: Setting[];

    iterableDiffer: any;

    firstEnter = 0;

    width: number = 500;
    height: number = 400;

    // keyboard
    deltaX = 0;
    deltaY = 0;
    keyEnd: true;

    // animations
    angle = 0;
    doAnimate: boolean;
    requestID;


    @HostListener('mouseover', ['$event']) onMouseOver(ev: MouseEvent) {
        this.rendered.setStyle(this.canvas.nativeElement, 'box-shadow', '5px 10px 18px #888888');

    };
    @HostListener('mouseleave') onMouseLeave(event: MouseEvent) {
        this.rendered.setStyle(this.canvas.nativeElement, 'box-shadow', 'none');

    };

    @HostListener('mousemove', ['$event'])
    onMousemove(event: MouseEvent) {

        let mouseX = event.clientX;
        let mouseY = event.clientY;
        let rect = this.canvas.nativeElement.getBoundingClientRect();
        let x = Math.floor(mouseX - rect.left);
        let y = mouseY - rect.top;

        let canvasEl: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d");

        this.clear();
        canvasEl.fillStyle = "#FF6A6A";
        canvasEl.fill();
        canvasEl.beginPath();
        canvasEl.arc(x, y, 10, 0, 2 * Math.PI);
        canvasEl.restore();

        if (x >= 150 && x <= 350 && y > 100 && y <= 300) {
            this.firstEnter++;

            canvasEl.fill();
            canvasEl.fillStyle = '#6cb3cc';
            canvasEl.beginPath();
            canvasEl.fillRect(150, 100, 200, 200);
            canvasEl.lineWidth = 1;
            canvasEl.stroke();
            canvasEl.save();

            if (this.firstEnter === 1) {
                this.requestID = requestAnimationFrame(() => this.animateCircle());
            }


        } else {
            this.firstEnter = 0;
            cancelAnimationFrame(this.requestID);
        }
    };

    @HostListener('document: keydown', ['$event'])
    handleKeyDown(event: KeyboardEvent) {

        if (event.key === 'ArrowUp') {
            if (this.deltaY > -70) {
                this.deltaY -= 2;
                event.preventDefault();
            }

        }
        if (event.key === 'ArrowDown') {
            if (this.deltaY < 90) {
                this.deltaY += 2;
                event.preventDefault();
            }

        };
        if (event.key === 'ArrowLeft') {
            if (this.deltaX > -60) {
                this.deltaX -= 2;
                event.preventDefault();
            }

        };
        if (event.key === 'ArrowRight') {
            if (this.deltaX < 60) {
                this.deltaX += 2;
                event.preventDefault();
            }

        }

        if (this.setting.objectType === 'Grouping') {
            if (this.setting.settings[2].defaultValue === true) {
                this.deltaX = 0;
                this.deltaY = 0;
            }
        };
    };

    constructor(private _iterableDiffers: IterableDiffers, private rendered: Renderer2) {
        this.iterableDiffer = this._iterableDiffers.find([]).create(null);   
    }

    ngOnInit() {

    }

    public ngDoCheck() {
        let changes = this.iterableDiffer.diff(this.myArray); 


        if (this.setting.objectType === 'Symbol') {
            this.draw();

        };

        if (this.setting.objectType === 'Connection') {
            this.connectionDraw();

        };

        if (this.setting.objectType === 'Grouping') {
            this.grouppingDraw();
        };
    }


    draw() {
        if (this.setting.objectType === 'Symbol') {
            let canvasEl: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d");
            canvasEl.save();
            this.clear();

            canvasEl.fillStyle = this.setting.settings[5].defaultValue;
            canvasEl.fill();
            canvasEl.beginPath();
            canvasEl.rect(150, 100, 200, 200);

            canvasEl.lineWidth = 20;
            canvasEl.strokeStyle = this.setting.settings[5].defaultValue;
            let styleCorners = this.setting.settings[6].defaultValue;
            if (styleCorners === true) {
                canvasEl.lineJoin = "round";
            } else {
                canvasEl.lineJoin = "miter"
            };

            canvasEl.stroke();

            if (this.setting.settings[1].defaultValue == null) {
                this.setting.settings[1].defaultValue = '';
            };

            canvasEl.fillStyle = "black";
            let fontSize = this.setting.settings[4].defaultValue;
            let fontStyle = this.setting.settings[3].defaultValue;
            let fontGroup = fontSize + "px" + " " + fontStyle;
            let font = fontGroup.toString();
            canvasEl.font = font;

            let textPosition = this.setting.settings[2].defaultValue;
            let message = this.setting.settings[1].defaultValue;
            if (textPosition == 'left') {
                canvasEl.textAlign = 'left';
                canvasEl.fillText(message, this.width / 2 - 90, this.height / 2);
            };
            if (textPosition == 'right') {
                canvasEl.textAlign = 'right';
                canvasEl.fillText(message, this.width / 2 + 90, this.height / 2);
            };
            if (textPosition == 'top') {
                canvasEl.fillText(message, this.width / 2 - ((canvasEl.measureText(message).width) / 2), this.height / 2 - 70);
                canvasEl.textBaseline = 'middle';
                canvasEl.textAlign = 'start';
            };
            if (textPosition == 'bottom') {
                canvasEl.fillText(message, this.width / 2 - ((canvasEl.measureText(message).width) / 2), this.height / 2 + 90);
                canvasEl.textBaseline = 'top';
            };
            if (textPosition == 'center') {
                canvasEl.fillText(message, this.width / 2 - ((canvasEl.measureText(message).width) / 2), this.height / 2);
                canvasEl.textAlign = 'center';
                canvasEl.textBaseline = 'middle';
            };

            canvasEl.restore();
        };

    };




    grouppingDraw() {
        if (this.setting.objectType === 'Grouping') {
            let canvasEl: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d");
            canvasEl.save();
            this.clear();

            canvasEl.fill();
            canvasEl.beginPath();
            canvasEl.fillRect(150, 100, 200, 200);
            canvasEl.lineWidth = 1;
            canvasEl.stroke();


            // yellow triangle
            canvasEl.beginPath();
            canvasEl.moveTo(230 + this.deltaX, 200 + this.deltaY);
            canvasEl.lineTo(270 + this.deltaX, 200 + this.deltaY);
            canvasEl.lineTo(250 + this.deltaX, 180 + this.deltaY);
            canvasEl.closePath();

            canvasEl.lineWidth = 10;
            canvasEl.strokeStyle = "rgba(102, 102, 102, 1)";
            canvasEl.stroke();

            canvasEl.fillStyle = "rgba(255, 204, 0, 1)";
            canvasEl.fill();


            // simple triangle
            canvasEl.beginPath();
            canvasEl.moveTo(230, 205);
            canvasEl.lineWidth = 1;
            if (this.setting.settings[3].defaultValue === 'dashed') {
                canvasEl.setLineDash([5]);

            } else if (this.setting.settings[3].defaultValue === 'dotted') {
                canvasEl.setLineDash([1, 2]);

            } else {
                canvasEl.stroke();

            };
            canvasEl.lineTo(270, 205);
            canvasEl.lineTo(250, 255);
            canvasEl.closePath();
            canvasEl.stroke();

            canvasEl.beginPath();
            canvasEl.arc(350, 130, 40, 0, 2 * Math.PI);
            canvasEl.lineWidth = 0;
            canvasEl.fillStyle = this.setting.settings[1].defaultValue;
            canvasEl.fill();

            canvasEl.restore();
        }

    };

    connectionDraw() {

        if (this.setting.objectType === 'Connection') {
            let canvasEl: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d");
            canvasEl.save();
            this.clear();

            canvasEl.fill();
            canvasEl.beginPath();
            canvasEl.fillRect(150, 100, 200, 200);
            canvasEl.lineWidth = 1;
            canvasEl.stroke();

            let arrowStart = this.setting.settings[2].defaultValue;

            if (arrowStart === true) {
                canvasEl.lineWidth = 1;
                canvasEl.strokeStyle = this.setting.settings[1].defaultValue;
                canvasEl.beginPath();
                canvasEl.moveTo(this.width / 2, 100);
                canvasEl.lineTo(245, 115);
                canvasEl.stroke();

                canvasEl.lineWidth = 1;
                canvasEl.strokeStyle = this.setting.settings[1].defaultValue;
                canvasEl.beginPath();
                canvasEl.moveTo(this.width / 2, 100);
                canvasEl.lineTo(255, 115);
                canvasEl.stroke();
            };

            let arrowEnd = this.setting.settings[3].defaultValue;
            if (arrowEnd === true) {
                canvasEl.lineWidth = 1;
                canvasEl.strokeStyle = this.setting.settings[1].defaultValue;
                canvasEl.beginPath();
                canvasEl.moveTo(200, 300);
                canvasEl.lineTo(195, 285);
                canvasEl.stroke();

                canvasEl.lineWidth = 1;
                canvasEl.strokeStyle = this.setting.settings[1].defaultValue;
                canvasEl.beginPath();
                canvasEl.moveTo(200, 300);
                canvasEl.lineTo(205, 285);
                canvasEl.stroke();
            };


            if (this.setting.settings[4].defaultValue == 'dashed') {
                canvasEl.setLineDash([5]);
                canvasEl.stroke();
            }; if (this.setting.settings[4].defaultValue == 'dotted') {
                canvasEl.setLineDash([1, 2]);
                canvasEl.stroke();
            };
            if (this.setting.settings[4].defaultValue == 'solid') {
                canvasEl.stroke();
            }
            canvasEl.strokeStyle = this.setting.settings[1].defaultValue;
            canvasEl.beginPath();
            canvasEl.moveTo(this.width / 2, 100);
            canvasEl.lineTo(this.width / 2, 200);
            canvasEl.lineTo(200, 200);
            canvasEl.lineTo(200, 300);
            canvasEl.stroke();

            canvasEl.restore();

        }

    };

    clear() {
        let canvasEl: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d");
        canvasEl.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    };

    animateCircle() {
        if (this.setting.objectType === 'Symbol') {
            let canvasEl: CanvasRenderingContext2D = this.canvas.nativeElement.getContext("2d");
            // this.clear();
            this.requestID = window.requestAnimationFrame(() => this.animateCircle());
            if (this.doAnimate = true) {

                canvasEl.beginPath();
                var radius = 25 + 80 * Math.abs(Math.cos(this.angle));
                canvasEl.arc(250, 200, radius, 0, Math.PI * 2, false);
                canvasEl.closePath();

                canvasEl.fillStyle = "#006699";
                canvasEl.fill();

                this.angle += Math.PI / 64;
            }
            else {
                cancelAnimationFrame(this.requestID);
            }
        }

    };

}

