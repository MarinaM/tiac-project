import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateWindowComponent } from './generate-window.component';

describe('GenerateWindowComponent', () => {
  let component: GenerateWindowComponent;
  let fixture: ComponentFixture<GenerateWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
