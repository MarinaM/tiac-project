import { Component, OnInit, Input } from '@angular/core';
import { Element } from '../../model/element';

@Component({
  selector: 'app-live-view',
  templateUrl: './live-view.component.html',
  styleUrls: ['./live-view.component.css']
})
export class LiveViewComponent implements OnInit {
  @Input() element:Element;

  constructor() { }

  ngOnInit() {
  }

}
