import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { Setting } from '../../models/setting'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  settings: Setting[];

  currentSettingId: string;

  currentSetting: Setting = new Setting(null, null, []);

  
  newObject: any[];
  currentObjectId: string;

  isVisible: boolean;
  isDisplayed: boolean = true;
  isChecking: boolean;

  
  constructor(private settingService: SettingsService) { }

  ngOnInit() {
    this.settingService.getSettings().subscribe(settings => {
      this.settings = settings;
    });

    // get new objects
    this.settingService.getObject().subscribe(settings => {
      this.newObject = settings;
    });

  }

  onSettingChange() {
    this.currentSetting = this.settings.filter(setting => setting._id === this.currentSettingId).shift();
  }
  onObjectChange() {
    this.currentSetting = this.newObject.filter(setting => setting._id === this.currentObjectId).shift();
  }

  saveSetting() {
    this.settingService.saveSetting(this.currentSetting).subscribe(setting => {
      alert('Changes are saved!');
    });
   
    window.location.reload(true);
  }

  addObject() {
    let settingObject = Object.assign({}, this.currentSetting);
    this.settingService.addObject(settingObject).subscribe(settings => {
      alert('New object is added!');
    });
    window.location.reload(true);
  }
  deleteObject(currentObjectId) {
    this.settingService.deleteObject(currentObjectId).subscribe(setting => {
      alert('Object is deleted!')
    });
    this.settings.splice(currentObjectId, 1);
    window.location.reload(true);
  }

  addNew() {
    this.isVisible = true;
    this.isDisplayed = false;

  }

}
