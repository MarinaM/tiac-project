import { Component, OnInit } from '@angular/core';
import { Element } from './model/element';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  data: Element[] = [
    new Element('Div', '#464646', '50px', '100px', '5px', '20px', 'yellow', '20px', 500, '10px')
  ];
 
 ngOnInit(){
 
 }
}

