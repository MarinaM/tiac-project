export class Element {
    public tag_name: string;
    public color: string;
    public height: string;
    public width: string;
    public border_width:string;
    public border_radius:string;
    public background_color: string;
    public font_size: string;
    public font_weight: number;
    public padding: string;

    constructor(tag_name?:string, color?:string, height?:string, width?:string, border_width?:string, border_radius?:string, background_color?:string, font_size?:string, font_weight?:number, padding?:string){
        this.tag_name = tag_name;
        this.color = color;
        this.height = height;
        this.width = width;
        this.border_width = border_width;
        this.border_radius = border_radius;
        this.background_color = background_color;
        this.font_size = font_size;
        this.font_weight = font_weight;
        this.padding = padding;

    }
}
