import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TableViewComponent } from './components/table-view/table-view.component';
import { FormViewComponent } from './components/form-view/form-view.component';
import { LiveViewComponent } from './components/live-view/live-view.component';
import { ColorPickerModule } from 'ngx-color-picker';
import {FormsModule} from '@angular/forms';
import { ColorComponent } from './components/form-view/color/color.component';
import { TextComponent } from './components/form-view/text/text.component';
import { NumberComponent } from './components/form-view/number/number.component';
import { LineStyleComponent } from './components/setting-type/line-style/line-style.component';
import { PositionComponent } from './components/setting-type/position/position.component';
import { IntegerComponent } from './components/setting-type/integer/integer.component';
import { FontComponent } from './components/setting-type/font/font.component';
import { BooleanComponent } from './components/setting-type/boolean/boolean.component';
import { TestComponent } from './components/test/test.component';
import { SettingsService } from './services/settings.service';
import { MainComponent } from './components/main/main.component';
import { GenerateWindowComponent } from './components/generate-window/generate-window.component';
import { TableWindowComponent } from './components/table-window/table-window.component';
import { FormWindowComponent } from './components/form-window/form-window.component';
import { AttributesService } from './services/attributes.service';
import { OrderModule } from 'ngx-order-pipe';

@NgModule({
  declarations: [
    AppComponent,
    TableViewComponent,
    FormViewComponent,
    LiveViewComponent,
    ColorComponent,
    TextComponent,
    NumberComponent,
    LineStyleComponent,
    PositionComponent,
    IntegerComponent,
    FontComponent,
    BooleanComponent,
    TestComponent,
    MainComponent,
    GenerateWindowComponent,
    TableWindowComponent,
    FormWindowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ColorPickerModule,
    HttpClientModule,
    OrderModule
  ],
  providers: [SettingsService, AttributesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
