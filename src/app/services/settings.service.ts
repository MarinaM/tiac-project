import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { Setting} from '../models/setting';




@Injectable({
  providedIn: 'root'
})
export class SettingsService {
private settings: Setting[]=[];
private newSettings: any[]=[];

  constructor(private http: HttpClient ) { }

  
  public getSettings(){
    return new Observable((o: Observer<any>) => {
      this.http.get('http://localhost:4000/settings').subscribe((settings:any)=> {
        let me = this;
        settings.forEach(setting => {
          me.settings.push(new Setting(
            setting._id,
            setting.objectType,
            setting.settings
          ));
        });

        o.next(this.settings);
        return o.complete();
      }, (err:HttpErrorResponse)=>{
        alert(`Backend returned code ${err.status} with message: ${err.error.error}`);
      });
    });
  }

  public getSettingsById(settingId:string){
    return new Observable((o:Observer<any>)=> {
      this.http.get(`http://localhost:4000/settings/${settingId}`).subscribe((setting:any)=>{
        o.next(new Setting(
          setting._id,
          setting.objectType,
          setting.settings
        ));
        return o.complete();
      }, (err: HttpErrorResponse) => {
                  alert(`Backend returned code ${err.status} with message: ${err.error.error}`);
                }
    );
    });
  }

  // saveSetting (setting: Setting): Observable<Setting> {
  //   return new Observable((o: Observer<any>) => {
  //     this.http.post('http://localhost:4000/setting/update/' + setting._id, {
  //       objectType: setting.objectType,
  //       settings: setting.settings
  //     }).subscribe((setting: any) => {
  //       o.next(setting);

  //       return o.complete();
  //     }, (err: HttpErrorResponse) => {
  //       alert(`Backend returned code ${err.status} with message: ${err.error}`);
  //     });
  //   })
  // }

    saveSetting (settingObject: Setting): Observable<Setting> {
    return new Observable((o: Observer<any>) => {
      this.http.post('http://localhost:4000/settingObjects/update/' +settingObject._id, {
        objectType: settingObject.objectType,
        settings: settingObject.settings
      }).subscribe((setting: any) => {
        o.next(setting);

        return o.complete();
      }, (err: HttpErrorResponse) => {
        alert(`Backend returned code ${err.status} with message: ${err.error}`);
      });
    })
  }

  addObject (settingObject: Setting): Observable<Setting>{
    return new Observable((o: Observer<any>) => {
      this.http.post('http://localhost:4000/settingObjects/add', {
        _id: null,
        objectType: settingObject.objectType,
        settings: settingObject.settings
      }).subscribe((settingObject: any) => {
        o.next(settingObject);

        return o.complete();
      }, (err: HttpErrorResponse) => {
        alert(`Backend returned code ${err.status} with message: ${err.error}`);
      });
    });
  };

  getObject(){
    return new Observable((o: Observer<any>) => {
          this.http.get('http://localhost:4000/settingObjects').subscribe((settingObject:any)=> {
            let me = this;
            settingObject.forEach(settingObject => {
              me.newSettings.push(new Setting(
                settingObject._id,
                settingObject.objectType,
                settingObject.settings
              ));
            });
    
            o.next(this.newSettings);
            return o.complete();
          }, (err:HttpErrorResponse)=>{
            alert(`Backend returned code ${err.status} with message: ${err.error.error}`);
          });
        });

  }

deleteObject(idObject: string): Observable<Setting>{
      return new Observable((o:Observer<any>) => {
        this.http.get(`http://localhost:4000/settingObjects/${idObject}/delete`).subscribe((settingObject: any) => {
          o.next(settingObject);
          return o.complete();
        }, (err: HttpErrorResponse) => {
          alert(`Backend returned code ${err.status} with message: ${err.error}`);
        });
      })
  };

  
}
