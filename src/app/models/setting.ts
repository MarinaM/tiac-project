import { SettingAtribute } from './setting-atribute'
export class Setting {
    _id: string;
    objectType: string;
    settings : SettingAtribute[] = [];

    constructor(id?: string, objectType?:string, settings?:SettingAtribute[]) {
        this._id = id;
        this.objectType = objectType;

        let me = this;

        settings.forEach(settingAttribute => {
            me.settings.push(new SettingAtribute(
                settingAttribute.name,
                settingAttribute.title,
                settingAttribute.type,
                settingAttribute.defaultValue,
                settingAttribute.minValue,
                settingAttribute.maxValue
            ));
        });
    }
}

