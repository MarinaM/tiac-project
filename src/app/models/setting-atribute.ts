export class SettingAtribute {
    public name: string;
    public title: string;
    public type : string;
    public defaultValue: any;
    public minValue: Number;
    public maxValue: Number;

    constructor(name?:string, title?:string, type?:string, defaultValue?:any, minValue?:Number, maxValue?:Number){
        this.name = name;
        this.title = title;
        this.type = type;
        this.defaultValue = type === 'Boolean' ? defaultValue === 'true' : defaultValue;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

}
