import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import Setting from './models/Setting';
import SettingObject from './models/SettingObject';


const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json());
mongoose.connect('mongodb://localhost:27017/tiac');
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

router.route('/settings').get((req, res) => {
    Setting.find((err, settings) => {
        if (err)
            console.log(err);
        else
            res.json(settings);
    });
});

router.route('/settings/:id').get((req, res) => {
    Setting.findById(req.params.id, (err, setting) => {
        if (err)
            console.log(err);
        else
            res.json(setting);
    });
});

router.route('/settingObjects/add').post((req, res) => {
    let settingObject = new SettingObject(req.body);
    settingObject.save()
        .then(settingObject => {
            res.status(200).json({'settingObject': 'Added successfully'});
        })
        .catch(err => {
            res.status(400).send(req.body);
            //res.status(400).send('Failed to create new record');
        });
});

router.route('/settingObjects/update/:id').post((req, res) => {
    SettingObject.findById(req.params.id, (err, setting) => {
        if (!setting)
            return next(new Error('Could not load Document'));
        else {
            setting.objectType = req.body.objectType;
            setting.settings = req.body.settings;
            setting.save().then(setting => {
                res.json('Update done');
            }).catch(err => {
                res.status(400).send('Update failed');
            });
        }
    });
});

router.route('/settingObjects').get((req, res) => {
    SettingObject.find((err, settingObjects) => {
        if (err)
            console.log(err);
        else
            res.json(settingObjects);
    });
});

router.route('/settingObjects/:id').get((req, res) => {
    SettingObject.findById(req.params.id, (err, settingObject) => {
        if (err)
            console.log(err);
        else
            res.json(settingObject);
    });
});

app.get('/settingObjects/:id/delete', function(req, res){
	SettingObject.findByIdAndRemove({_id: req.params.id}, 
	   function(err, docs){
		if(err) res.json(err);
		else    res.json('Removed successfully');
	});
});


app.use('/', router);
app.listen(4000, () => console.log(`Express server running on port 4000`));