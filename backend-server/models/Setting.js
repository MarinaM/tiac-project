import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let Setting = new Schema({
    _id: {
        type: Schema.Types.ObjectId
    },
    objectType:{
        type: String
    },
    settings:[
        {
            name:{
                type: String 
             },
             title:{
                 type: String
             },
             type: {
                 type: String
             },
             defaultValue: {
                 type: String
             },
             minValue:{
                 type: Number
             },
             maxValue:{
                 type: Number
             }
        }
    ]
});


export default mongoose.model('Setting', Setting);
