import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let SettingObject = new Schema({
    _id: {
        type: Schema.Types.ObjectId
    },
    objectType:{
        type: String
    },
    settings:[
        {
            name:{
                type: String 
             },
             title:{
                 type: String
             },
             type: {
                 type: String
             },
             defaultValue: {
                 type: String
             },
             minValue:{
                 type: Number
             },
             maxValue:{
                 type: Number
             }


        }
    ]
});

export default mongoose.model('SettingObject', SettingObject, 'settingObjects');
