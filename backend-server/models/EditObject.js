import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let EditObject = new Schema({
    objId: {
        type:Schema.Types.ObjectId
    },
    objectType: {
        type: String
    },
    settings: {
        type: any
    }
});

export default mongoose.model('EditObject', EditObject);