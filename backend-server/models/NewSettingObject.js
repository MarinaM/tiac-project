import mongoose from 'mongoose';
const Schema = mongoose.Schema;
let NewSettingObject = new Schema({
  _id: {
    type: Schema.Types.ObjectId
  },
  objectType: {
    type: String
  },
  settings: {
    type: String,
    alias: 'title'
  }

});

export default mongoose.model('NewSettingObject', NewSettingObject, 'newSettingObjects');
